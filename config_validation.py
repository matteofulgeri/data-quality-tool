import input_output as IO
import constants
import jsonschema 
import sys

def validate(FILE_NAME):
    configuration = IO.load_configuration(FILE_NAME)
    schema = IO.load_configuration(constants.SCHEMA_FILE)
    try: 
        jsonschema.validate(configuration,schema)
    except jsonschema.exceptions.ValidationError as ve:
        sys.exit(str(ve) + "\n")
    try: 
        to_be_joined_counter(configuration)
        is_primary_counter(configuration)
        output_in_definition(configuration)
        second_axis_measure_in_definition(configuration)
        save_input_in_source_data(configuration)
        preprocessing_in_soruce_data(configuration)
        data_sources_in_preprocessing_exists(configuration)
        input_definition(configuration)
        not_empty_connection_string(configuration)
        graph_number_of_measure(configuration)
        graph_aggregation_function_per_measure(configuration)
    except ValueError as err:
        sys.exit(err)

# No more than 2 data sources can be listed as "to_be_joined"
def to_be_joined_counter(configuration):
    to_be_joined_counter = 0
    for source in configuration['source_data']:
        if source['to_be_joined'] =='True':
            to_be_joined_counter = to_be_joined_counter+1
    if to_be_joined_counter != 2: 
        raise ValueError("Error in the json semanthic:\n\"to_be_joined\" flag needs to be \"True\" in exactly 2 data soruce")
        
# No more than 1 data sources can be listed as "is_primary"
def is_primary_counter(configuration):
    is_primary_counter = 0
    for source in configuration['source_data']:
        if source['is_primary'] =='True':
            is_primary_counter = is_primary_counter+1 
    
    if is_primary_counter > 1: 
        raise ValueError("Error in the json semanthic:\n\"is_primary\" flag needs to be \"True\" maximum once")
        
           
# All the dimension, measures and lists, needs to exist in the analysis definition
def output_in_definition(configuration):
    output_list = [] 
    for table in configuration['output']['sheets']:
        output_list.extend(table['dimensions'])
        output_list.extend(table['measures'])
        output_list.extend(table['lists'])
        
    output_list = list(set(output_list)) #converting as set will deduplicate the dimension list
    
    definition_list = [] 
    for definition in configuration['analysis_definitions']:
        definition_list.append(definition['label'])
    
    definition_list = list(set(definition_list)) 
    
    if len(set(output_list) - set(definition_list)) > 0:
        raise ValueError("Error in the json semanthic:\n configuration[output] is containing unknown elements: " + str(set(output_list) - set(definition_list)))


def second_axis_measure_in_definition(configuration):
    output_list = []
    for table in configuration['output']['sheets']:
        element = table['options']['second_axis_measure']
        if len(element) > 0: 
            output_list.append(next(iter(element)))
        
    definition_list = [] 
    for definition in configuration['analysis_definitions']:
        definition_list.append(definition['label'])
    
    definition_list = list(set(definition_list)) 
    
    if len(set(definition_list) - set(output_list)) != (len(set(definition_list)) - len(set(output_list))) :
        raise ValueError("Error in the json semanthic:\n configuration[output][..]['second_axis_measure'] is containing unknown elements: " + str(set(output_list) - set(definition_list)))

# All the source names in Save input, needs to exist in the input_data
def save_input_in_source_data(configuration):
    input_list = [] 
    for input in configuration['execution']['create_backup_zip']['save_input']:
        input_list.append(input['source_name'])
        
    source_list = [] 
    for source in configuration['source_data']:
        source_list.append(source['source_name'])
        
    if len(set(input_list) - set(source_list)) > 0:
        raise ValueError("Error in the json semanthic:\nconfiguration[execution][save_input] is containing unknown elements: " + str(set(input_list) - set(source_list)))


# All the preprocessing data soruces names, needs to exist in the input_data
def preprocessing_in_soruce_data(configuration):
    preproc_list = [] 
    
#    preproc = configuration['pre_processing'][0]
    for preproc in configuration['pre_processing']['python_code']:
        preproc_list = preproc_list + preproc['data_sources']
        
    preproc_list = list(set(preproc_list))
    
    source_list = [] 
    for source in configuration['source_data']:
        source_list.append(source['source_name'])
        
    if len(set(preproc_list) - set(source_list)) > 0:
        raise ValueError("Error in the json semanthic:\nconfiguration[pre_processing][][data_sources] is containing unknown elements: " + str(set(preproc_list) - set(source_list)))

def input_definition(configuration):
    for source in configuration['source_data']:
        if source['source_type'] not in list(source.keys()):
            raise ValueError("Error in the json semanthic:\n)" + source['source_name'] + " should be " + source['source_type'] + " but there are no details")

def not_empty_connection_string(configuration):
#   data_soruce = configuration['source_data'][0]
    for data_soruce in configuration['source_data']:
        if data_soruce['source_type'] == 'text' and ( len(data_soruce['text']['path']) == 0  or len(data_soruce['text']['text_separator']) == 0 ) :
            raise ValueError("Connection string cannot be empty: configuration[source_data][text]")
        
        if data_soruce['source_type'] == 'binary' and ( len(data_soruce['binary']['path']) == 0 ) :
            raise ValueError("Connection string cannot be empty: configuration[source_data][binary]")
        
        if data_soruce['source_type'] == 'excel' and ( len(data_soruce['excel']['path']) == 0  or len(data_soruce['excel']['excel_sheet']) == 0 ) :
            raise ValueError("Connection string cannot be empty: configuration[source_data][excel]")
        
        if data_soruce['source_type'] == 'db' and ( len(data_soruce['db']['host']) == 0  or len(data_soruce['db']['driver']) == 0  or len(data_soruce['db']['port']) == 0  or len(data_soruce['db']['database']) == 0 or len(data_soruce['db']['username']) == 0 or len(data_soruce['db']['sql_code']) == 0  ) :
            raise ValueError("Connection string cannot be empty: configuration[source_data][db]")
        
        if data_soruce['source_type'] == 'script' and len(data_soruce['script']['path']) == 0:
            raise ValueError("Connection string cannot be empty: configuration[source_data][script]")    
        

#Checking if the data soruces mentioned in the preprocessing step are existing
def data_sources_in_preprocessing_exists(configuration):
    existing_data_source = set([])
    for defined_data_source in configuration['source_data']:
        existing_data_source.add(defined_data_source['source_name'])

    for preprocessing_operation in configuration['pre_processing']['python_code']:
        if len(preprocessing_operation['data_sources']) > 0 and len(set(preprocessing_operation['data_sources']) - existing_data_source) > 0:
            raise ValueError("Data source mentioned in [preprocessing] is not defined \n"+ str(preprocessing_operation))
            
def graph_number_of_measure(configuration):
    for id, config in enumerate(configuration['output']['sheets']):
        if config['sheet_type'] == 'graph':
                if len(config['measures']) > 2: 
                    raise ValueError("Graph cannot have more than 2 measures: configuration[output][sheets]" + str([id]) + '[measures]')
                    
def graph_aggregation_function_per_measure(configuration):
    for id, config in enumerate(configuration['output']['sheets']):
        if config['sheet_type'] == 'graph':
            for measure_list in list(config['measures'].keys()):
                if len(config['measures'][measure_list]) > 1: 
                    raise ValueError("Graph cannot have more than 1 aggregation function per measure: configuration[output][sheets]" + str([id]) + '[measures]')                    
