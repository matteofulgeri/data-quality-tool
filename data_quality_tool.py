import datetime
import os
import sys
import time 
import numpy as np
import pandas as pd
import xlsxwriter
import constants
import input_output as IO
import utilities as util
import config_validation as v

def main(FILE_NAME):
    global configuration
    global data
    
#    FILE_NAME = "./Phase 2/0_configuration/saslar_HPA_RXA_dev.json"
    t = util.start_clock("Executing ", FILE_NAME, message_end='\\n')
    
    v.validate(FILE_NAME)
    
    # Reading configuration file
    configuration = IO.load_configuration(FILE_NAME)
    
    output_file_name = configuration['output']['excel_name'] + " - " + datetime.datetime.today().strftime('%Y-%m-%d %H.%M.%S')
    backup_file_name = configuration['execution']['create_backup_zip']['path'] + os.path.basename(output_file_name)

    # Loading data sources
    data = IO.load_data(configuration)
    
    # Saving Input Data
    IO.create_backup(backup_file_name, FILE_NAME, configuration, data)  
    
    # Data preprocessing
    preprocessing(configuration)
    
    # adding suffix to headers of data to avoid ambiguity
    util.add_suffix(data, configuration)  
    
    # Parsing dates
    util.parse_dates(configuration)  
    
    # Generating output results
    output(output_file_name, FILE_NAME)
    
    util.stop_clock(t, message='Total Execution: ')

def preprocessing(configuration):
    #Read python code from configuration file and execute it.
    t = util.start_clock("- Preprocessing", message_end='\\n')
    if configuration['execution']['transform'] == 'True':
        #pre_processing = configuration['pre_processing']['python_code'][0]
        for pre_processing in configuration['pre_processing']['python_code']:
            if pre_processing['active'] == 'True':
                t = util.start_clock("   Processing:", pre_processing['comment'])
                source_name = []
                # Genarate the list if data soruces to copy for the preprocessing activity.
                if len(pre_processing['data_sources']) == 0:
                    # if the list is empty, the system will  create a copy of all the available data sources
                    for i in range(0, len(configuration['source_data'])):
                        source = configuration['source_data'][i]['source_name']
                        code = 'source_name.append(source)'
                        exec(code)
                else:
                    # Otherwise the system will copy just the selected list
                    for idx, val in enumerate(pre_processing['data_sources']):
                        code = 'source_name.append(pre_processing[\'data_sources\'][idx])'
                        exec(code)

                preproc_code = compile(pre_processing["code"], '<string>', 'exec')

                #Generating a local copy of the data to mantain the atomicity of the operations
                namespace = {}
                for idx, val in enumerate(source_name):
                    code = 'namespace.update({\'' + val + '\':data[\'' + val + '\'].copy()})'
                    exec(code)

                try:
                    exec(preproc_code, globals(), namespace)
                    for idx, val in enumerate(source_name):
                        data[val] = namespace[val].copy()

                except:
                    print("Error occured during executing pre-processing codes")
                    sys.exit(-1)
                util.stop_clock(t)
                
        #pre_processing = configuration['pre_processing']['script'][0]
        for pre_processing in configuration['pre_processing']['script']:
            if pre_processing['active'] == 'True':
                t = util.start_clock("   Processing:", pre_processing['comment'])
                source_name = []
                # Genarate the list if data soruces to copy for the preprocessing activity.
                for i in range(0, len(configuration['source_data'])):
                    source = configuration['source_data'][i]['source_name']
                    code = 'source_name.append(source)'
                    exec(code)
                
                namespace = {}
                for idx, val in enumerate(source_name):
                    code = 'namespace.update({\'' + val + '\':data[\'' + val + '\'].copy()})'
                    exec(code)

                try:
                    exec(open(pre_processing['path']).read(),globals(), namespace)
                    # exec(preproc_code, globals(), namespace)
                    for idx, val in enumerate(source_name):
                        data[val] = namespace[val].copy()
                        
                except:
                    print("Error occured during executing pre-processing codes")
                    sys.exit(-1)
                util.stop_clock(t)
                
    #Application of the the standard transformations
    if configuration['pre_processing']['dimension_trim'] == 'True':
        for data_source in configuration['source_data']:
            source_name = data_source['source_name'] 
            dimention_list = str(util.list_of_field_by_type(configuration, source_name, 'dimension'))
            exec( 'data[\'' + source_name + '\']' + '['  +  dimention_list + '] = ' + 'data[\'' + source_name + '\']' + '['  +  dimention_list + '].applymap(lambda x: x.strip())' )

    if configuration['pre_processing']['dimension_upper_case'] == 'True':
        for data_source in configuration['source_data']:
            source_name = data_source['source_name'] 
            dimention_list = str(util.list_of_field_by_type(configuration, source_name, 'dimension'))
            exec( 'data[\'' + source_name + '\']' + '['  +  dimention_list + '] = ' + 'data[\'' + source_name + '\']' + '['  +  dimention_list + '].applymap(lambda x: x.upper())' )

    if configuration['pre_processing']['dimension_remove_duplicate_space'] == 'True':
            exec( 'data[\'' + source_name + '\']' + '['  +  dimention_list + '] = ' + 'data[\'' + source_name + '\']' + '['  +  dimention_list + '].applymap(lambda x: " ".join(x.split()))' )
        
    if configuration['pre_processing']['dimension_cast_to_string'] == 'True':
        for data_source in configuration['source_data']:
            source_name = data_source['source_name'] 
            dimention_list = str(util.list_of_field_by_type(configuration, source_name, 'dimension'))
            exec( 'data[\'' + source_name + '\']' + '['  +  dimention_list + '] = ' + 'data[\'' + source_name + '\']' + '['  +  dimention_list + '].astype(str)' )
    
    if configuration['pre_processing']['dimension_fill_null'] == 'True':
        for data_source in configuration['source_data']:
            source_name = data_source['source_name'] 
            dimention_list = str(util.list_of_field_by_type(configuration, source_name, 'dimension'))
            exec( 'data[\'' + source_name + '\']' + '['  +  dimention_list + '] = ' + 'data[\'' + source_name + '\']' + '['  +  dimention_list + '].fillna("")' )

    if configuration['pre_processing']['measure_cast_to_foat'] == 'True':
        for data_source in configuration['source_data']:
            source_name = data_source['source_name'] 
            dimention_list = str(util.list_of_field_by_type(configuration, source_name, 'measure'))
            exec( 'data[\'' + source_name + '\']' + '['  +  dimention_list + '] = ' + 'data[\'' + source_name + '\']' + '['  +  dimention_list + '].applymap(lambda x: float(x))' )
    
    for data_source in data:
        if data[data_source].size == 0 :
            raise ValueError("After the preprocessing, at least one data source is now empty")
            sys.exit()

    
def output(output_file_name, FILE_NAME):
    if configuration['execution']['output']['create_output'] == 'True':
        excel_file_name = output_file_name + ".xlsx"
        excelWriter = pd.ExcelWriter(excel_file_name, engine='xlsxwriter')
        
        t = util.start_clock("- Generating output tables", message_end='\\n')
        
        # Generating the first page containing overall information
        full_joined_table = full_join(data, configuration)
        overall_sheet(full_joined_table, excelWriter)
    
        # Calculating aggregated output using full join and condition provided in the configuration file
        # output_definition  = configuration['output']['sheets'][0]
        for output_definition in configuration['output']['sheets']:
            if output_definition['active'] == 'True':
                sheet_name = util.cleanup_excel_sheetname(output_definition['sheet_name'])
                t1 = util.start_clock("   ", variable=str(sheet_name))
                
                # Preparing the output table
                output = create_output_data(output_definition, full_joined_table, configuration)
                
                # Writing down the output in excel 
                write_output_to_excel(output, excelWriter, output_definition, sheet_name = sheet_name)
                
                # Generating the graph if necessary 
                if output_definition['sheet_type'] == 'graph':
                    generate_chart(output, excelWriter, output_definition, sheet_name = sheet_name)
                
                util.stop_clock(t1)

        if configuration['execution']['output']['basic_output_generation']['active'] == 'True':
            t = util.start_clock("- Generating base output", message_end='\\n')
            if configuration['execution']['output']['basic_output_generation']['aggregation_type'] == 'aggregate':
                ### looping over all the analysis definition where attribute_type is True and active is true
                ### construct a output definition with default values 
                
#               definition = configuration['analysis_definitions']
                temp_output_def_empty = {'sheet_name': '','sheet_type': '','options': {'hide_table':'','aggregation_type':'', 'conditional_formatting':'','enable_filters':'','freeze_first_line':'','freeze_dimension':''},'active': '','dimensions': [],'measures': {},'lists': []}
                
                for definition in (definition for definition in configuration['analysis_definitions'] if definition['attribute_type'] == 'dimension' and definition['active'] == 'True'):
                    dimension = definition['label']
                    t1 = util.start_clock("   ", variable=str(dimension))
                       
                    temp_output_def = temp_output_def_empty
                    measures = {}

                    for measure in (definition for definition in configuration['analysis_definitions'] if definition['attribute_type'] == 'measure' and definition['active'] == 'True'):
                        measures[measure['label']] = ['sum']

                    temp_output_def['sheet_name'] = util.cleanup_excel_sheetname(dimension)
                    temp_output_def['sheet_type'] = 'table'
                    temp_output_def['active'] = 'true'
                    temp_output_def['dimensions'] = [dimension]
                    temp_output_def['measures'] = measures # TODO: add list of measures with default sum
                    temp_output_def['options']['aggregation_type'] = 'aggregate'
                    temp_output_def['options']['hide_table'] = constants.BASIC_OUTPUT_GENERATION_HIDE_TABLE_FOR_GRAPH
                    temp_output_def['options']['conditional_formatting'] = constants.BASIC_OUTPUT_GENERATION_CONDITIONAL_FORMATTING
                    temp_output_def['options']['enable_filters'] = constants.BASIC_OUTPUT_GENERATION_ENABLE_FILTERS
                    temp_output_def['options']['freeze_first_line'] = constants.BASIC_OUTPUT_GENERATION_FREEZE_FIRST_LINE
                    temp_output_def['options']['freeze_dimension'] = constants.BASIC_OUTPUT_GENERATION_FREEZE_DIMENSION
                    
                    output = create_output_data(temp_output_def, full_joined_table, configuration)
                    
                    write_output_to_excel(output, excelWriter, temp_output_def, sheet_name = temp_output_def['sheet_name'])
                    
                    if configuration['execution']['output']['basic_output_generation']['generate_graphs'] == 'True':
                        temp_output_def['sheet_type'] = 'graph'
                        generate_chart(output, excelWriter, temp_output_def, sheet_name = temp_output_def['sheet_name'])
                    
                    util.stop_clock(t1)
            
            if configuration['execution']['output']['basic_output_generation']['aggregation_type'] == 'full-join':
                for definition in (definition for definition in configuration['analysis_definitions'] if definition['attribute_type'] == 'dimension' and definition['active'] == 'True'):
                    dimension = definition['label']
                    t1 = util.start_clock("   ", variable=str(dimension))

                    temp_output_def = temp_output_def_empty
                    measures = {}

                    for measure in (definition for definition in configuration['analysis_definitions'] if definition['attribute_type'] == 'measure' and definition['active'] == 'True'):
                        measures[measure['label']] = ['sum']
                    
                    temp_output_def['sheet_name'] = util.cleanup_excel_sheetname(dimension)
                    temp_output_def['options']['aggregation_type'] = 'full-join'
                    temp_output_def['active'] = 'true'
                    temp_output_def['dimensions'] = [dimension]
                    temp_output_def['measures'] = measures # TODO: add list of measures with default sum
                    
                    temp_output_def['sheet_type'] = 'table'
                    temp_output_def['options']['hide_table'] = constants.BASIC_OUTPUT_GENERATION_HIDE_TABLE_FOR_GRAPH
                    temp_output_def['options']['conditional_formatting'] = constants.BASIC_OUTPUT_GENERATION_CONDITIONAL_FORMATTING
                    temp_output_def['options']['enable_filters'] = constants.BASIC_OUTPUT_GENERATION_ENABLE_FILTERS
                    temp_output_def['options']['freeze_first_line'] = constants.BASIC_OUTPUT_GENERATION_FREEZE_FIRST_LINE
                    temp_output_def['options']['freeze_dimension'] = constants.BASIC_OUTPUT_GENERATION_FREEZE_DIMENSION
                    
                    output = create_output_data(temp_output_def, full_joined_table, configuration)
                        
                    write_output_to_excel(output, excelWriter, temp_output_def, sheet_name = temp_output_def['sheet_name'])
                    
                    if configuration['execution']['output']['basic_output_generation']['generate_graphs'] == 'True':
                        temp_output_def['sheet_type'] = 'graph'
                        generate_chart(output, excelWriter, temp_output_def, sheet_name = temp_output_def['sheet_name'])
                    util.stop_clock(t1)

        # generating the unmatched tables from the full join
        if configuration['execution']['output']['save_unmatched'] == 'True':
            unmatched_list = create_unmatched_record_table(configuration, data, full_joined_table)
            write_unmatch_to_excel(unmatched_list, excelWriter, FILE_NAME)

        finalize_excel(excelWriter)


def create_structure_for_dimension_as_join(output_definition):
    """
    The method is generating the data structure "data_to_join" 
    containing all the data necessary to proceed for abtain the final table
    """
    data_to_join = []
        
    # Group the original input data at the level of aggregation specified in the output definiton (dimension)
    tables_to_join = []
    for data_source in configuration['source_data']:
        if data_source['to_be_joined'] == 'True':
           tables_to_join.append(data_source['source_name'])

    #data_source_name = tables_to_join[0]
    for data_source_name in tables_to_join:
        data_to_group = data[data_source_name]
        
        # Addinct colum to calculate the numbber of lines
        data_to_group['counting_columns'] = 1 
        
        # Defining a strucutre where to store all the information to be used for the join
        table_information = {"source_name": None, "grouped_data": None, "join_condition":None, "aggregation_functions":None, "columns_for_delta":None}
        
        # List of fields to be used in the the group by
        dimension_list = []
        for dimension in output_definition['dimensions']:
            dimension_list.append(util.definition_to_original_column_name(configuration,data_source_name, dimension))
        
        # List of labels to be used for renaming (dimension)
#        renaming_columns_dimension = []
#        for dimension in output_definition['dimensions']:
#            renaming_columns_dimension.append(dimension)
        # List of labels to be used for renaming (dimension) new version
        renaming_columns_dimension = output_definition['dimensions']
        
        #List of Elements to be used with the "list" feature
        list_list = []
        for list_unit in output_definition['lists']:
            list_list.append(util.definition_to_original_column_name(configuration,data_source_name, list_unit))
        
        renaming_columns_list = []
        for list_unit in output_definition['lists']:
            renaming_columns_list.append('List of '+ list_unit + ' ' + data_source_name)
        
        renaming_columns_measures = []
        renaming_columns_for_delta = []
        
        join_condition = []
        for dimension in output_definition['dimensions']:
            join_condition.append(dimension)
        
        renaming_columns_measures.append("#Records - " + data_source_name)
        renaming_columns_for_delta.append("#Records")
        
        # setup the logic for the aggregation functions
        agg_list = []
        agg_list.append({"counting_columns": sum})
        
        measure_dictionary = output_definition['measures']
        
        if output_definition['sheet_type'] == 'graph' and output_definition['options']['second_axis_measure'] != {}:
            measure_dictionary.update(output_definition['options']['second_axis_measure'])
        
        for key in measure_dictionary.keys():
            aggregation_field = util.definition_to_original_column_name(configuration, data_source_name, key)
            aggregation_function_list = measure_dictionary[key]
            for aggregation_function in aggregation_function_list:
                agg_list.append({aggregation_field: getattr(np,aggregation_function)})
                column_label = util.label_to_definition_single_source(aggregation_field, configuration)
                column_name = (aggregation_function.title() + " " + column_label + " - " + data_source_name)#.title()
                renaming_columns_measures.append(column_name)
                renaming_columns_for_delta.append(column_label)

        # Group up the tables and calculation the aggregation function 
        # (for each aggregation function, a table will be generated) 
        grouped_data = []
        index = 0
        for aggregation_function in agg_list:
            data_grouped = data_to_group.groupby(dimension_list).agg(aggregation_function).reset_index()
            data_grouped.columns = renaming_columns_dimension + [renaming_columns_measures[index]]
            grouped_data.append(data_grouped)
            index = index +1 
            
        # setup the logic for the list 
        #Generating the list of values available based on the level of aggregation
        grouped_data_for_list = []
        index = 0
        #unit  = 'Product_Legacy'
        for unit in list_list:
            data_grouped = pd.DataFrame(data_to_group.groupby(dimension_list).agg(unit).apply(np.unique)).reset_index() 
            data_grouped.columns = renaming_columns_dimension + [renaming_columns_list[index]]
            grouped_data_for_list.append(data_grouped)
            index = index +1 
        
        # Storing all usefull information in the data structure 
        table_information["source_name"] = data_source_name
        table_information["grouped_data"] = grouped_data
        table_information["grouped_data_for_list"] = grouped_data_for_list
        table_information["join_condition"] = join_condition
        table_information["aggregation_functions"] = agg_list
        table_information["columns_for_delta"] = renaming_columns_for_delta
        
        # All the data to be joined are available in the data structure 
        data_to_join.append(table_information)

    #checkging that 
    left_sizes = []
    right_sizes = []
    
    left_tables = data_to_join[0]['grouped_data']
    right_tables = data_to_join[1]['grouped_data']
    
    #The shape of the dataframes contained into data_to_join needs to be the same. 
    for dataframe in left_tables: 
        left_sizes.append(dataframe.size)
        
    for dataframe in right_tables: 
        right_sizes.append(dataframe.size)
    
    if right_sizes == left_sizes :
        return data_to_join
    else:
        raise ValueError("Shape of the data is not matching and cannot be joined")
        sys.exit()



def join_dimension_as_join(data_to_join, output_definition):
    """
    The method is reading the information contained in the data structure "data_to_join"
    joinding all the tables together
    generating also the delta values for all the aggregation functions
    """
    # Joining the preaggregated tables
    left_data = data_to_join[0]
    right_data = data_to_join[1]
    
    joined_tables = []
    
    #joining the table based on the dimension
    for index in range(0,len(left_data['grouped_data'])):
        joined_table = pd.merge(left_data['grouped_data'][index],
                            right_data['grouped_data'][index],
                            how="outer",
                            left_on=left_data['join_condition'],
                            right_on=right_data['join_condition'])
        
        #Generating Delta columns
        left_value = joined_table[joined_table.columns[-2]]
        right_value = joined_table[joined_table.columns[-1]]
        
        Column_name = "Δ " + left_data['columns_for_delta'][index] + " (" + left_data['source_name'] + " vs " + right_data['source_name'] + ")"
        joined_table[Column_name] = left_value - right_value
        
        #Generating Delta % columns
        Column_name = "Δ% " + left_data['columns_for_delta'][index] + " (" + left_data['source_name'] + " vs " + right_data['source_name'] + ")"
        joined_table[Column_name] = left_value / right_value
        
        joined_tables.append(joined_table)
    

    for index in range(0,len(left_data['grouped_data_for_list'])):
        joined_table = pd.merge(left_data['grouped_data_for_list'][index],
                            right_data['grouped_data_for_list'][index],
                            how="outer",
                            left_on=left_data['join_condition'],
                            right_on=right_data['join_condition'])
        
        #Set of Elements available into the left part of the table 
        left_set = set(joined_table[joined_table.columns[-2]][0])
        right_set = set(joined_table[joined_table.columns[-1]][0])
        
        #Set of Elements available into the left part of the table
#        delta_set = pd.DataFrame(columns = ['Delta'])
        
        
#        right_set = {'201711', '201806', '201811', '201812', '201903', '201808', '201801', '201901', '201805', '201802', '201810', '201803', '201809', '201712'}
#        delta_set = left_set - right_set
#        
#        if len(delta_set) == 0:
#            delta_set['Delta'] = 'All Matching'
#        else: 
#            delta_set['Delta'] = str(left_set - right_set)
#        
#        joined_tables.append(joined_table)
        
        #Generating Delta columns for lists
        
        
    
    final_table = joined_tables[0]
    for table in joined_tables[1:]:
        columns_to_drop = left_data['join_condition']
        table = table.drop(columns_to_drop, axis = 1 )
        final_table = pd.merge(final_table, table, left_index=True, right_index=True, how='inner')
        
    return final_table
  
def create_output_data(output_definition, full_joined_table, configuration):
    if output_definition['options']['aggregation_type'] in ('aggregate'):
        # this branch is implementing the generation of the tables using the list of dimension output of the report 
        data_to_join = create_structure_for_dimension_as_join(output_definition)
        final_table = join_dimension_as_join(data_to_join, output_definition)
    else: 
        # this branch is implementing the generation of the output tables using as 
        # a join condition, the full list of dimension as specified in analysis definition.    
        
        #looking up the couple of tables that needs to be joined
        tables_to_join = []
        for data_source_name in configuration['source_data']:
            if data_source_name['to_be_joined'] == 'True':
                tables_to_join.append(data_source_name)
                
        # simple aggregation list for counting
        agg_dimension = {'source_both': sum}
        for data_source_name in tables_to_join:
            if data_source_name['to_be_joined'] == 'True':
                agg_dimension.update({'source_' + data_source_name['source_name']: sum})    
        
        # extend the aggregation list above to include agg function SUM, MIN, MAX, etc.
        agg_measure_label = {}
        agg_measure_col = {}
        
        for measure in output_definition["measures"]:
            agg_funcs_list = [getattr(np, func) for func in output_definition["measures"][measure]]
            for data_source_name in tables_to_join:
                if data_source_name['to_be_joined'] == 'True':
                    analysis_def = next(x for x in configuration['analysis_definitions'] if x['label'] == measure)
                    source_col_name = next(x for x in analysis_def['source_columns'] if x['source_name'] == data_source_name['source_name'])
                    agg_measure_col.update({source_col_name['source_column']: agg_funcs_list})
                    agg_measure_label.update({source_col_name['source_column']: measure + '_' + data_source_name['source_name']})
    
        # Creating renaming template for the aggregation of dimensions
        renaming_dim = {'source_both': "# Records in both"}
        for data_source_name in tables_to_join:
            if data_source_name['to_be_joined'] == 'True':
                renaming_dim.update(
                    {'source_' + data_source_name['source_name']: '# Records in ' + data_source_name['source_name'] + " only"})
    
        # Mapping the dimension listed in the configuration table, with the actual columns names
        lists = util.definition_to_labels(output_definition['lists'], configuration)
    
        # Create list of DataFrames, each of them is an inner_join with existing join conditions and one more extra join condition.
        inner_tables = []
        list_extra_joins = util.create_extra_join_conditions(configuration)
        for join_extra in list_extra_joins:
            inner_table = extra_full_join(join_extra)
            inner_tables.append(inner_table)
        
        # creating a new column that is implementing "ISNULL" in SQL for each dimension declared in the configuration file
        columns_names_aggr = [s + '_agg' for s in output_definition['dimensions']]
        for dimension in output_definition['dimensions']:
            dimension_pairs = util.definition_to_labels([dimension], configuration)
            full_joined_table[dimension + "_agg"] = full_joined_table[dimension_pairs[0]].fillna(full_joined_table[dimension_pairs[1]])
            for inner_table in inner_tables:
                inner_table[dimension + "_agg"] = inner_table[dimension_pairs[0]].fillna(inner_table[dimension_pairs[1]])
    
        # aggregate by dimensions
        groupby_dimension = full_joined_table.groupby(columns_names_aggr).agg(agg_dimension).rename(columns=renaming_dim)
    
        # calculate the percentage of records in each sources
        left = tables_to_join[0]['source_name']
        right = tables_to_join[1]['source_name']
        col_left = '# Records in ' + left + " only"
        col_right = '# Records in ' + right + " only"
        col_both = "# Records in both"
    
        total_records = groupby_dimension[col_both] + groupby_dimension[col_left] + groupby_dimension[col_right]
        # calculating statistis on the grouped table: % of records
        groupby_dimension["% Records in both"] = round(groupby_dimension[col_both] / total_records, 2)
        groupby_dimension["% Records in " + left + " only"] = round(groupby_dimension[col_left] / total_records, 2)
        groupby_dimension["% Records in " + right + " only"] = round(groupby_dimension[col_right] / total_records, 2)
    
        # For each table from the list of DataFrames with inner_joins do groupby. 
        # calculate difference between number of records in both tables and a newly created table with inner_join.
        i = 0
        while i < len(inner_tables):
            groupby_inner_table = inner_tables[i].groupby(columns_names_aggr).agg({'source_both': sum})
            groupby_dimension["#unmaching - Join Attributes + " + util.names_extra_joins(configuration)[i]] = groupby_dimension["# Records in both"] - groupby_inner_table[
                "source_both"]
            groupby_dimension["#unmaching - Join Attributes + " + util.names_extra_joins(configuration)[i]].fillna(value="N/A", inplace=True)
            i += 1
    
        # aggregate the measures
        if agg_measure_col:
            groupby_measure = full_joined_table.groupby(columns_names_aggr).agg(agg_measure_col).swaplevel(axis=1).rename(columns=agg_measure_label,
                                                                                                                     level=1).sort_index(axis=1)
            # collapse multiindex column names into one level only
            groupby_measure.columns = ['_'.join(col).rstrip('_') for col in groupby_measure.columns.values]
    
            # get name of primary and secondary source from config file
            tablelist = []
            sources = {}
            for source in configuration["source_data"]:
                if source["to_be_joined"] == "True" and source["is_primary"] == "True":
                    sources = dict(sources, **{"primary_source": source["source_name"]})
                elif source["to_be_joined"] == "True" and source["is_primary"] == "":
                    sources = dict(sources, **{"secondary_source": source["source_name"]})
                
            # loop over the groupby_measure table and calculate the delta
            for measure in output_definition["measures"]:
                for func in output_definition["measures"][measure]:
                    # select column based on measure name and aggregate function
                    delta_table = groupby_measure.loc[:, groupby_measure.columns.str.contains(func + '_' + measure)].copy()
    
                    # build name of the primary col
                    primary_col = func + '_' + measure + '_' + sources['primary_source']
                    primary_col_new = func.capitalize() + ' ' + measure + ' - ' + sources['primary_source']
    
                    # build name of secondary col
                    secondary_col = func + '_' + measure + '_' + sources['secondary_source']
                    secondary_col_new = func.capitalize() + ' ' + measure + ' - ' + sources['secondary_source']
    
                    # calculate delta and percentage delta
                    delta_col_name = 'Δ ' + func + ' ' + measure
                    delta_table.loc[:, delta_col_name] = delta_table[secondary_col] - delta_table[primary_col]
    
                    delta_percent_col_name = 'Δ% ' + func + ' ' + measure
                    delta_table.loc[:, delta_percent_col_name] = (delta_table[secondary_col] - delta_table[primary_col]) / delta_table[primary_col]
    
                    # rename column
                    delta_table.rename(columns={primary_col: primary_col_new, secondary_col: secondary_col_new}, inplace=True)
                    tablelist.append(delta_table)
    
            groupby_measure = pd.concat(tablelist, axis=1)
            tables_to_merge = [pd.concat([groupby_dimension, groupby_measure], axis=1)]
        else:
            tables_to_merge = [groupby_dimension]
    
        # calculating the part of the table that is listing the distinct values
        # Casting to string all the elements of the full_joined dataframe except for the counting columns
        cols_names = full_joined_table.columns.tolist()
        cols_names.remove('source_both')
        for key in data.keys():
            cols_names.remove('source_' + key)
    
        # Filling null values null with "" to overcome joining problems
        joined_table_copy = full_joined_table.copy()
        joined_table_copy[cols_names] = joined_table_copy[cols_names].fillna("")
    
        for element_in_list in output_definition['lists']:
            renaming_list = {}
            lists = util.definition_to_labels([element_in_list], configuration)
            unique_values_couples = []
            for unit_list in lists:
                # Creation of single columns table with unique values
                unique = pd.DataFrame(joined_table_copy[columns_names_aggr + lists].groupby(columns_names_aggr)[unit_list].apply(np.unique))
    
                # tables are create in couples and then merged together
                unique_values_couples.append(unique)
    
                # updating the list of columsn that needs to be renamed
                renaming_list.update({unit_list: 'Unique Values - ' + unit_list})
    
            # merging the couple of columns that are listing unique values and rename headers
            paired_table = pd.concat(unique_values_couples, axis=1).rename(columns=renaming_list)
    
            # calculating string differences
            renamed_col_left = list(renaming_list.values())[0]
            renamed_col_right = list(renaming_list.values())[1]
            comparison_col_name = element_in_list + " differences"
    
            paired_table[comparison_col_name] = paired_table.apply(
                lambda row: (set(row[renamed_col_left]) ^ set(row[renamed_col_right])), axis=1)
    
            column_name_left = '# Elements - ' + renamed_col_left.replace('Unique Values - ', '')
            column_name_right = '# Elements - ' + renamed_col_right.replace('Unique Values - ', '')
    
            paired_table[column_name_left] = paired_table.apply(lambda row: len(set(row[renamed_col_left])), axis=1)
            paired_table[column_name_right] = paired_table.apply(lambda row: len(set(row[renamed_col_right])), axis=1)
    
            column_name_difference = '# Elements of difference - ' + element_in_list
    
            paired_table[column_name_difference] = paired_table.apply(
                lambda row: len((set(row[renamed_col_left]) ^ set(row[renamed_col_right]))), axis=1)
    
            paired_table[renamed_col_left] = paired_table[renamed_col_left].str.join(", ")
            paired_table[renamed_col_right] = paired_table[renamed_col_right].str.join(", ")
            paired_table[comparison_col_name] = paired_table[comparison_col_name].str.join(", ")
    
            # adding the table to the list of tables to merge with initial groupby_table
            tables_to_merge.append(paired_table)
    
        # merging the results with the main aggregation (groupby_table), when necessary
        if lists:
            final_table = pd.concat(tables_to_merge, axis=1).rename(columns=renaming_list).reset_index()
        else:
            final_table = tables_to_merge[0].reset_index()
    
        # cleanup of column header (removing the "_aggr" suffix by the groupby columns)
        renamed_columns = list(final_table.columns)
        for i in range(0, len(renamed_columns)):
            renamed_columns[i] = renamed_columns[i].replace('_agg', '')
        final_table.columns = renamed_columns
        final_table.sort_values(
            by=["% Records in both", "% Records in " + left + " only", "% Records in " + right + " only"], ascending=False,
            inplace=True)
    
    return final_table
    

def generate_chart(output, excelWriter, output_definition, sheet_name):
    left = 0
    right = 1
    workbook = excelWriter.book
    
    if output_definition['options']['graph_type'] == "":
        output_definition['options']['graph_type'] = constants.BASIC_OUTPUT_GENERATION_GRAPH_TYPE
    
    # If the table to draw is 1 line only, line chart will result in no picture. forcing the visualization type to "bar"
    if len(output) == 1 and output_definition['options']['graph_type'] == "line":
        output_definition['options']['graph_type'] = 'bar'
        print("\n    Warning: chart type has been forced to <bar> for sheet " + output_definition['sheet_name'])
    chart = workbook.add_chart({'type': output_definition['options']['graph_type']})
    
    max_report_lenght = len(output)
    chartsheet = workbook.add_chartsheet(util.cleanup_excel_sheetname(sheet_name+' chart'))
    
    max_colum_for_dimension = util.int_to_excel_column(len(output_definition['dimensions']))

#    if output_definition['options']['graph_type'] != 'line': 
#        border_color = 'white'
#    else:
#        border_color = 'black'

    for measure in list(output_definition['measures'].keys()):
        if (output_definition['options']['second_axis_measure'] == {}) or (measure != list(output_definition['options']['second_axis_measure'].keys())[0]):
            column_names = util.measure_to_column_names(output_definition,output,measure)    
            if output_definition['options']['data_labels_for_graph'] == 'True':
                chart.add_series({
                    'name':       '=' + '\'' + sheet_name + '\'' + '!' + '$'+ column_names[left] + '$1',
                    'values':     '=' + '\'' + sheet_name + '\'' + '!' + '$'+ column_names[left] + '$2:$'+ column_names[left] +'$' + str(max_report_lenght+1), 
                    'data_labels': {'value': 1},
                    'categories': '=' + '\'' + sheet_name + '\'' + '!' + '$A$2:$'+ max_colum_for_dimension +'$' + str(max_report_lenght+1)
                })
                chart.add_series({
                    'name':       '=' + '\'' + sheet_name + '\'' + '!' + '$'+ column_names[right] + '$1',
                    'values':     '=' + '\'' + sheet_name + '\'' + '!' + '$'+ column_names[right] + '$2:$'+ column_names[right] +'$' + str(max_report_lenght+1), 
                    'data_labels': {'value': 1},
                    'categories': '=' + '\'' + sheet_name + '\'' + '!' + '$A$2:$'+ max_colum_for_dimension +'$' + str(max_report_lenght+1)
                })
            else: 
                chart.add_series({
                    'name':       '=' + '\'' + sheet_name + '\'' + '!' + '$'+ column_names[left] + '$1',
                    'values':     '=' + '\'' + sheet_name + '\'' + '!' + '$'+ column_names[left] + '$2:$'+ column_names[left] +'$' + str(max_report_lenght+1), 
                    'categories': '=' + '\'' + sheet_name + '\'' + '!' + '$A$2:$'+ max_colum_for_dimension +'$' + str(max_report_lenght+1)
                })
                chart.add_series({
                    'name':       '=' + '\'' + sheet_name + '\'' + '!' + '$'+ column_names[right] + '$1',
                    'values':     '=' + '\'' + sheet_name + '\'' + '!' + '$'+ column_names[right] + '$2:$'+ column_names[right] +'$' + str(max_report_lenght+1), 
                    'categories': '=' + '\'' + sheet_name + '\'' + '!' + '$A$2:$'+ max_colum_for_dimension +'$' + str(max_report_lenght+1)
                })
            
            if output_definition['options']['graph_type'] == 'column':
                chart.set_y_axis({'name': measure})
            elif output_definition['options']['graph_type'] == 'bar':
                chart.set_x_axis({'name': measure})
            elif output_definition['options']['graph_type'] == 'line':
                chart.set_x_axis({'name': measure})
        
    if output_definition['options']['second_axis_measure'] != {}:
        meaure = list(output_definition['options']['second_axis_measure'])[0]
        column_names = util.measure_to_column_names(output_definition,output,meaure)
        
        if output_definition['options']['second_axis_graph_type'] == "":
            output_definition['options']['second_axis_graph_type'] = constants.BASIC_OUTPUT_GENERATION_GRAPH_TYPE
        
        if len(output) == 1 and output_definition['options']['second_axis_graph_type'] == "line":
            output_definition['options']['second_axis_graph_type'] = output_definition['options']['graph_type']
            print("\n    Warning: chart type (second axis) has been forced to " + output_definition['options']['graph_type'] + " for sheet " + output_definition['sheet_name'])
            second_chart = workbook.add_chart({'type': output_definition['options']['second_axis_graph_type']})
        
        if output_definition['options']['data_labels_for_graph'] == 'True':
            second_chart.add_series({
                'name':       '=' + '\'' + sheet_name + '\'' + '!' + '$'+ column_names[left] + '$1',
                'values':     '=' + '\'' + sheet_name + '\'' + '!' + '$'+ column_names[left] + '$2:$'+ column_names[left] +'$' + str(max_report_lenght+1), 
                'categories': '=' + '\'' + sheet_name + '\'' + '!' + '$A$2:$'+ max_colum_for_dimension +'$' + str(max_report_lenght+1),
                'data_labels': {'value': 1},
                'y2_axis':    True
            })
            
            second_chart.add_series({
                'name':       '=' + '\'' + sheet_name + '\'' + '!' + '$'+ column_names[right] + '$1',
                'values':     '=' + '\'' + sheet_name + '\'' + '!' + '$'+ column_names[right] + '$2:$'+ column_names[right] +'$' + str(max_report_lenght+1), 
                'categories': '=' + '\'' + sheet_name + '\'' + '!' + '$A$2:$'+ max_colum_for_dimension +'$' + str(max_report_lenght+1),
                'data_labels': {'value': 1},
                'y2_axis':    True
            })
        else: 
            second_chart.add_series({
                'name':       '=' + '\'' + sheet_name + '\'' + '!' + '$'+ column_names[left] + '$1',
                'values':     '=' + '\'' + sheet_name + '\'' + '!' + '$'+ column_names[left] + '$2:$'+ column_names[left] +'$' + str(max_report_lenght+1), 
                'categories': '=' + '\'' + sheet_name + '\'' + '!' + '$A$2:$'+ max_colum_for_dimension +'$' + str(max_report_lenght+1),
                'y2_axis':    True
            })
            
            second_chart.add_series({
                'name':       '=' + '\'' + sheet_name + '\'' + '!' + '$'+ column_names[right] + '$1',
                'values':     '=' + '\'' + sheet_name + '\'' + '!' + '$'+ column_names[right] + '$2:$'+ column_names[right] +'$' + str(max_report_lenght+1), 
                'categories': '=' + '\'' + sheet_name + '\'' + '!' + '$A$2:$'+ max_colum_for_dimension +'$' + str(max_report_lenght+1),
                'y2_axis':    True
            })
        second_chart.set_y2_axis({'name': measure})
        chart.combine(second_chart)
    
    chart.set_legend({'position': 'bottom'})
    
    chartsheet.set_chart(chart)
    chartsheet.set_tab_color(constants.SHEET_COLOR_GRAPHICS)
    chartsheet.set_zoom(120)
    
    if output_definition['options']['hide_table'] == 'True':
        # Hiding the original sheet that has benn used for generate this graph
        worksheet = excelWriter.sheets[sheet_name]
        worksheet.hide()

def write_output_to_excel(output, excelWriter, output_definition, sheet_name):
    if output_definition['options']['aggregation_type'] in ('aggregate'):
        
        #layout to be used for the dimension as join type of output
        workbook = excelWriter.book
        
        
        
        # formatting style
        normal_style = workbook.add_format(constants.STYLE_NORMAL)
        dim_style = workbook.add_format(dict(constants.STYLE_HEADER, **{'bg_color': constants.REPORT_COLOR_DIM}))
        measure_style = workbook.add_format(dict(constants.STYLE_HEADER, **{'bg_color': constants.REPORT_COLOR_MEASURE}))
        measure_style_alternative = workbook.add_format(dict(constants.STYLE_HEADER, **{'bg_color': constants.REPORT_COLOR_MEASURE_ALTERNATIVE}))
        num_record_style = workbook.add_format(dict(constants.STYLE_HEADER, **{'bg_color': constants.REPORT_COLOR_INNER}))
        list_style = workbook.add_format(dict(constants.STYLE_HEADER, **{'bg_color': constants.REPORT_COLOR_LIST}))
        percentage_style = workbook.add_format(dict(constants.STYLE_HEADER, **{'bg_color': constants.REPORT_COLOR_PERCENTAGE}))
        inner_style = workbook.add_format(dict(constants.STYLE_HEADER, **{'bg_color': constants.REPORT_COLOR_INNER}))
        header_style = workbook.add_format(dict(constants.STYLE_HEADER, **{'bg_color': constants.REPORT_COLOR_HEADER}))
        percent_format = workbook.add_format(constants.FORMAT_PERCENTAGE)
        number_format = workbook.add_format(constants.FORMAT_NUMBER_DECIMALS)
        number_nocomma_format = workbook.add_format(constants.FORMAT_NUMBER_NOCOMMA)
    
        #cutting the sheet name in case is too long
        sheet_name = util.cleanup_excel_sheetname(sheet_name)
    
        output.to_excel(excelWriter, sheet_name, index=False)
        worksheet = excelWriter.sheets[sheet_name]
    
        worksheet.conditional_format(
            "$A$1:${0}${1}".format(xlsxwriter.utility.xl_col_to_name(len(output.columns)), len(output.index) + 1),
            {"type": "no_blanks", "format": normal_style})
        
        #Identify the position of the columns containing the number of records 
        first_column_num_records = 0
        
        for col_num, value in enumerate(output.columns.values):
            if (("#Records" in value) & (first_column_num_records == 0)):
                  first_column_num_records = col_num
                  
        # Identify the position of the first columns with measures
        first_column_measures = first_column_num_records + 4
        
        # Used to alternate the colors of the block of measures 
        measure_counter = 0
        
        for col_num, value in enumerate(output.columns.values):
            if col_num < first_column_num_records: 
                worksheet.write(0, col_num, value, header_style)
            elif col_num >= first_column_num_records and col_num < first_column_measures:
                worksheet.write(0, col_num, value, num_record_style)
                if (col_num - first_column_num_records ) == 3: #correspond to the last columns of the #Records block
                    worksheet.set_column('{0}:{0}'.format(xlsxwriter.utility.xl_col_to_name(col_num)), None, percent_format)
                else:
                    worksheet.set_column('{0}:{0}'.format(xlsxwriter.utility.xl_col_to_name(col_num)), None, number_nocomma_format)
            elif col_num >= first_column_measures:
                if (col_num - first_column_measures+1) % 4 == 0 : #correspond to the last columns of the #a block a measure block
                    worksheet.set_column('{0}:{0}'.format(xlsxwriter.utility.xl_col_to_name(col_num)), None, percent_format)
                else:    
                    worksheet.set_column('{0}:{0}'.format(xlsxwriter.utility.xl_col_to_name(col_num)), None, number_format)
                
                if (col_num - first_column_measures) % 4 == 0 :
                    if measure_counter == 0:
                        measure_counter = 1
                    else:
                        measure_counter = 0
                if measure_counter == 0:
                    worksheet.write(0, col_num, value, measure_style)
                else:
                    worksheet.write(0, col_num, value, measure_style_alternative)
   
        worksheet.set_tab_color(constants.SHEET_COLOR_DIMENSION_AS_JOIN)
                
    elif output_definition['options']['aggregation_type'] in ('full-join'):
        #layout to be used when dimension as join is NOT True
        workbook = excelWriter.book
        # define formatting style
        normal_style = workbook.add_format(constants.STYLE_NORMAL)
        dim_style = workbook.add_format(dict(constants.STYLE_HEADER, **{'bg_color': constants.REPORT_COLOR_DIM}))
        measure_style = workbook.add_format(dict(constants.STYLE_HEADER, **{'bg_color': constants.REPORT_COLOR_MEASURE}))
        list_style = workbook.add_format(dict(constants.STYLE_HEADER, **{'bg_color': constants.REPORT_COLOR_LIST}))
        percentage_style = workbook.add_format(dict(constants.STYLE_HEADER, **{'bg_color': constants.REPORT_COLOR_PERCENTAGE}))
        inner_style = workbook.add_format(dict(constants.STYLE_HEADER, **{'bg_color': constants.REPORT_COLOR_INNER}))
        header_style = workbook.add_format(dict(constants.STYLE_HEADER, **{'bg_color': constants.REPORT_COLOR_HEADER}))
        percent_format = workbook.add_format(constants.FORMAT_PERCENTAGE)
        number_format = workbook.add_format(constants.FORMAT_NUMBER)
    
        output.to_excel(excelWriter, sheet_name, index=False)
        worksheet = excelWriter.sheets[sheet_name]
    
        worksheet.conditional_format(
            "$A$1:${0}${1}".format(xlsxwriter.utility.xl_col_to_name(len(output.columns)), len(output.index) + 1),
            {"type": "no_blanks", "format": normal_style})
        for col_num, value in enumerate(output.columns.values):
            if "# Records" in value:
                worksheet.write(0, col_num, value, dim_style)
            elif "% Records" in value:
                worksheet.write(0, col_num, value, percentage_style)
                worksheet.set_column('{0}:{0}'.format(xlsxwriter.utility.xl_col_to_name(col_num)), None, percent_format)
            elif "#unmaching" in value:
                worksheet.write(0, col_num, value, inner_style)
            elif any(x in value.lower() for x in ['min', 'max', 'mean', 'sum']):
                worksheet.write(0, col_num, value, measure_style)
                worksheet.set_column('{0}:{0}'.format(xlsxwriter.utility.xl_col_to_name(col_num)), None, number_format)
            elif any(x in value for x in ['Unique', 'differences', 'Elements']):
                worksheet.write(0, col_num, value, list_style)
            else:
                worksheet.write(0, col_num, value, header_style)
    
        for col_num, value in enumerate(output.columns.values):
            if "Δ%" in value:
                worksheet.set_column('{0}:{0}'.format(xlsxwriter.utility.xl_col_to_name(col_num)), None, percent_format)
            elif "Δ " in value:
                worksheet.set_column('{0}:{0}'.format(xlsxwriter.utility.xl_col_to_name(col_num)), None, number_format)
    
        worksheet.set_tab_color(constants.SHEET_COLOR_FULL_JOIN)
    
    # Resize columns    
    for i, width in enumerate(util.get_col_widths(output)):
        worksheet.set_column(i, i, width)
    
    # Freezing panes
    max_columns_dimension = len(output_definition['dimensions'])
    if output_definition['options']['freeze_first_line'] == 'True' and output_definition['options']['freeze_dimension'] == '':
        worksheet.freeze_panes(1, 0)
    elif output_definition['options']['freeze_first_line'] == '' and output_definition['options']['freeze_dimension'] == 'True':
        worksheet.freeze_panes(0, max_columns_dimension)
    elif output_definition['options']['freeze_first_line'] == 'True' and output_definition['options']['freeze_dimension'] == 'True':
        worksheet.freeze_panes(1, max_columns_dimension)
        
    # Enable autofilters
    num_lines = len(output)
    num_columns = len(output.columns)
    if output_definition['options']['enable_filters'] == 'True':
        worksheet.autofilter(0, 0, num_lines -1 , num_columns-1)
    
    # Condtional foormatting
    if output_definition['options']['conditional_formatting'] == 'True':
        #if there aren't any other measures, the conditional formatting is applied to #Records
        if len(output_definition['measures']) == 0:
            for idx, column in enumerate(list(output.columns)):
                # Format for the Δ measure (data bars)
                if column[0] == 'Δ' and column[0:2] == 'Δ ':
                    column_name = util.int_to_excel_column(idx+1)
                    range_to_use = column_name + '2:' + column_name + str(num_lines + 1)
                    format_color =  {'type': 'data_bar',
                                    'bar_color': constants.DATA_BAR_COLOR,
                                    'bar_negative_color': constants.DATA_BAR_COLOR,
                                    'bar_border_color': constants.DATA_BAR_BORDER,
                                    'bar_negative_border_color': constants.DATA_BAR_BORDER,
                                    'bar_axis_color': constants.DATA_BAR_AXIS_COLOR}
                    worksheet.conditional_format( range_to_use , format_color)
                # Format for the Δ% measure (icon set)
                if column[0] == 'Δ' and column[0:2] == 'Δ%':
                    print(column )
                    column_name = util.int_to_excel_column(idx+1)
                    range_to_use = column_name + '2:' + column_name + str(num_lines + 1)
                    format_color = {'type': 'icon_set',
                                    'icon_style': '5_arrows_gray',
                                        'icons': [{'criteria': '>=', 'type': 'number','value': 1 + constants.DATA_BAR_THRESHOLD_FOR_YELLOW_PERCENTAGE},
                                                  {'criteria': '>', 'type': 'number','value': 1},
                                                  {'criteria': '>=', 'type': 'number','value': 1},
                                                  {'criteria': '>=', 'type': 'number','value': 1 - constants.DATA_BAR_THRESHOLD_FOR_YELLOW_PERCENTAGE }]}
#                    format_color = {'type': '3_color_scale',
#                                    'mid_value':0,
#                                    'min_value':-0.00001,
#                                    'max_value':0.00001,
#                                    'mid_color':'#FFFFFF',
#                                    'min_color':constants.DATA_BAR_COLOR_FOR_PERCENTAGE,
#                                    'max_color':constants.DATA_BAR_COLOR_FOR_PERCENTAGE,
#                                    'min_type':'num',
#                                    'max_type':'num',
#                                    'mid_type':'num'}
                    worksheet.conditional_format( range_to_use , format_color)
        
        else: 
            #if there are other measures the conditional formatting is not considering the #Records 
            for idx, column in enumerate(list(output.columns)):
                # Format for the Δ measure (data bars)
                if column[0] == 'Δ' and column[0:2] != 'Δ%' and column[0:10] != 'Δ #Records':
                    column_name = util.int_to_excel_column(idx+1)
                    range_to_use = column_name + '2:' + column_name + str(num_lines + 1)
                    format_color =  {'type': 'data_bar',
                                    'bar_color': constants.DATA_BAR_COLOR,
                                    'bar_negative_color': constants.DATA_BAR_COLOR,
                                    'bar_border_color': constants.DATA_BAR_BORDER,
                                    'bar_negative_border_color': constants.DATA_BAR_BORDER,
                                    'bar_axis_color': constants.DATA_BAR_AXIS_COLOR}
                    worksheet.conditional_format( range_to_use , format_color)
                
                # Format for the Δ% measure (icon set)
                if column[0] == 'Δ' and column[0:2] == 'Δ%' and column[0:10] != 'Δ #Records' and column[0:11] != 'Δ% #Records':
                    column_name = util.int_to_excel_column(idx+1)
                    range_to_use = column_name + '2:' + column_name + str(num_lines + 1)
                    format_color = {'type': 'icon_set',
                                    'icon_style': '5_arrows_gray',
                                        'icons': [{'criteria': '>=', 'type': 'number','value': 1 + constants.DATA_BAR_THRESHOLD_FOR_YELLOW_PERCENTAGE},
                                                  {'criteria': '>', 'type': 'number','value': 1},
                                                  {'criteria': '>=', 'type': 'number','value': 1},
                                                  {'criteria': '>=', 'type': 'number','value': 1 - constants.DATA_BAR_THRESHOLD_FOR_YELLOW_PERCENTAGE }]}
#                    format_color = {'type': '3_color_scale',
#                                        'mid_value':0,
#                                        'min_value':-0.00001,
#                                        'max_value':0.00001,
#                                        'mid_color':'#FFFFFF',
#                                        'min_color':constants.DATA_BAR_COLOR_FOR_PERCENTAGE,
#                                        'max_color':constants.DATA_BAR_COLOR_FOR_PERCENTAGE,
#                                        'min_type':'num',
#                                        'max_type':'num',
#                                        'mid_type':'num'}
                    worksheet.conditional_format( range_to_use , format_color)
    
        
def write_unmatch_to_excel(unmatched_list, excelWriter, FILE_NAME):
    workbook = excelWriter.book
    # define formatting style
    normal_style = workbook.add_format(constants.STYLE_NORMAL)
    isjoin_style = workbook.add_format(dict(constants.STYLE_HEADER, **{'bg_color': constants.REPORT_COLOR_ISJOIN}))
    header_style = workbook.add_format(dict(constants.STYLE_HEADER, **{'bg_color': constants.REPORT_COLOR_HEADER}))

    for key, table in unmatched_list.items():
        sheetName = "Not matching " + key + " (all attr.)"
        
        if len(sheetName) > 31:
            sheetName = sheetName[0:19] + " (all attr.)"

        table.to_excel(excelWriter, sheetName, index=False)
        worksheet = excelWriter.sheets[sheetName]
        worksheet.set_tab_color(constants.SHEET_COLOR_NOT_MATCHING)

        # draw cell border on all non-blank cells
#        worksheet.conditional_format("$A$1:${0}${1}".format(xlsxwriter.utility.xl_col_to_name(len(table.columns)), len(table.index) + 1),
#                                     {"type": "no_blanks", "format": normal_style})
        # create dict of join condition for comparison
        dict_join_conditions = util.create_dict_join_conditions(IO.load_configuration(FILE_NAME))
        # use xlsxwriter to rewrite header with format
        for col_num, value in enumerate(table.columns.values):
            if value in dict_join_conditions[key]:
                worksheet.write(0, col_num, value, isjoin_style)
            else:
                worksheet.write(0, col_num, value, header_style)

        for i, width in enumerate(util.get_col_widths(table)):
            worksheet.set_column(i, i, width)

def finalize_excel(excelWriter):
    t = util.start_clock("- Writing Results: ")
    excelWriter.save()
    util.stop_clock(t)

def create_unmatched_record_table(configuration, data, joined_table):
    unmachted_list = {}
    # join_condition = util.create_dict_join_conditions(configuration)
    unmatched_elements = joined_table['_merge'].unique().tolist()
    if 'both' in unmatched_elements:
        unmatched_elements.remove('both')
    for not_matching in unmatched_elements:
        unmatched_rows = joined_table[joined_table['_merge'] == not_matching]
        # restricting the columns to the original data set
        column_selection = unmatched_rows.filter(like=not_matching).columns.tolist()
        if "numrecords_" + not_matching in column_selection:
            column_selection.remove("numrecords_" + not_matching)
        if "source_" + not_matching in column_selection:
            column_selection.remove("source_" + not_matching)
        unmatched_rows = unmatched_rows[column_selection]
        unmatched_rows.columns = unmatched_rows.columns.str.replace('_' + not_matching, '')
        unmachted_list[not_matching] = unmatched_rows
    return unmachted_list

#joined_table = full_joined_table
def overall_sheet(joined_table, excelWriter):
    """
    Creates an excel sheet "overall".
    the Overall sheet contains three tables:
    1. number of matches/% of matches between data sources
    2. name of matching cols between data sources
    3. data sources properties
    """
    # excel_writer = excelWriter
    # create new worksheet called "Overall" and put it into dict of worksheet
    workbook = excelWriter.book
    worksheet = workbook.add_worksheet('Overall')
    excelWriter.sheets['Overall'] = worksheet

    tables_to_join = []
    for data_source_name in configuration['source_data']:
        if data_source_name['to_be_joined'] == 'True':
            tables_to_join.append(data_source_name)

    # create data overlap table
    # first create a dict, each element in the dict is a column with element key is the column header
    # convert the dict into a df by calling pd.DataFrame.from_dict()
    table = joined_table.loc[joined_table['_merge'] == "both"]
    dict_overall = {'Join using all attributes': ["Matching between the Tables"],
                    'Number of Records': [len(table.index)],
                    '% of Records': ["{0:.1%}".format(len(table.index) / len(joined_table.index))]}

    for data_soruce_name in tables_to_join:
        dict_overall.setdefault("Join using all attributes", []).append("Not matching from " + data_soruce_name['source_name'] + " source")
        table_key = joined_table.loc[joined_table['_merge'] == data_soruce_name['source_name']]
        dict_overall.setdefault("Number of Records", []).append(len(table_key.index))
        dict_overall.setdefault("% of Records", []).append(
            "{0:.1%}".format(len(table_key.index) / len(joined_table.index)))

    df_overall = pd.DataFrame.from_dict(dict_overall)

    # create list of attr from both data sources
    dict_match_attr = {}
    for attr in configuration['analysis_definitions']:
        if attr['active'] == 'True':
            dict_match_attr.setdefault("Attribute Name", []).append(attr["label"])
            for source in attr['source_columns']:
                dict_match_attr.setdefault(source['source_name'], []).append(source['source_column'])
            dict_match_attr.setdefault("Attribute Type", []).append(attr["attribute_type"])


    df_match_attr = pd.DataFrame.from_dict(dict_match_attr)

    list_of_sources = list(data.keys())
    for source in list_of_sources:
        df_match_attr = df_match_attr.replace("_" + source, "", regex=True)

    # create list of source properties
    dict_source_prop = {"Data source": [],
                        "Is primary": [],
                        "Number of records": [],
                        "Format": [],
                        "Path": [],
                        "Data as of": [],
                        "Notes": []}
    
    # source = configuration['source_data'][0]
    for source in configuration['source_data']:
        dict_source_prop.setdefault("Data source", []).append(source['source_name'])
        dict_source_prop.setdefault("Is primary", []).append(source["is_primary"])
        dict_source_prop.setdefault("Number of records", []).append(len(data[source['source_name']].index))
        dict_source_prop.setdefault("Format", []).append(source['source_type'])
        if source['source_type'] == 'db':
            dict_source_prop.setdefault("Path", []).append(source['db']['sql_code'])
        elif source['source_type'] == 'excel':
            dict_source_prop.setdefault("Path", []).append(source['excel']['path'])
        elif source['source_type'] == 'text':
            dict_source_prop.setdefault("Path", []).append(source['text']['path'])
        elif source['source_type'] == 'binary':
            dict_source_prop.setdefault("Path", []).append(source['binary']['path'])
        elif source['source_type'] == 'script':
            dict_source_prop.setdefault("Path", []).append(source['script']['path'])
        dict_source_prop.setdefault("Data as of", []).append(source['data_as_of'])
        dict_source_prop.setdefault("Notes", []).append(source['notes'])

    df_source_prop = pd.DataFrame.from_dict(dict_source_prop)

    workbook = excelWriter.book
    worksheet = excelWriter.sheets['Overall']

    # Write the overall matching line between source
    # Detect primary data source
    sources = {}
    for source in configuration["source_data"]:
        if source["is_primary"] == "True":
            sources = dict(sources, **{"primary_source": source["source_name"]})
        else:
            sources = dict(sources, **{"secondary_source": source["source_name"]})
    # write overall record check:
    row_offset = 0  # number of row to push the position of subsequent table down the excel sheet
    if "primary_source" in sources:
        compare_num = len(table.index) / len(data[sources["primary_source"]].index)
        worksheet.write(0, 0, "{0:.2%}".format(compare_num) + " of the information of " + sources[
            "primary_source"] + " are present in " + sources[
                            "secondary_source"])
        row_offset += 1
    # write output notes
    if len(configuration["output"]["notes"]) > 0:
        worksheet.write(row_offset, 0, "Notes: " + configuration["output"]["notes"])
        row_offset += 1
    # padding empty row
    if row_offset > 0:
        row_offset += 1

    # writing to excel sheet and formatting
    # list of tables and their location on excel sheet
    # each list contains [table, position to write first row, position of last row]
    table_location = [[df_overall, row_offset, len(df_overall.index) + row_offset],
                      [df_match_attr, len(df_overall.index) + 2 + row_offset,
                       len(df_overall.index) + row_offset + len(df_match_attr) + 2],
                      [df_source_prop, len(df_overall.index) + len(df_match_attr) + 4 + row_offset,
                       len(df_overall.index) + len(df_match_attr) + len(df_source_prop) + 4 + row_offset]]

    for df in table_location:
        df[0].to_excel(excelWriter, sheet_name='Overall', startrow=df[1], index=False)

    # define format styles
    normal_style = workbook.add_format(constants.STYLE_NORMAL)
    isjoin_style = workbook.add_format(dict(constants.STYLE_NORMAL, **{'bg_color': constants.REPORT_COLOR_ISJOIN}))
    header_style = workbook.add_format(dict(constants.STYLE_HEADER, **{'bg_color': constants.REPORT_COLOR_HEADER}))

    # format column headers of each table
    for df in table_location:
        for col_num, value in enumerate(df[0].columns.values):
            worksheet.write(df[1], col_num, value, header_style)

    # use conditional format to format row where isjoin_condition = True
    worksheet.conditional_format("$A${0}:$D${1}".format(table_location[1][1] + 1, table_location[1][2] + 1),
                                 {"type": "formula",
                                  "criteria": '=INDIRECT("D"&ROW())="dimension"',
                                  "format": isjoin_style
                                  })
    # draw cell border on all non-blank cells
    worksheet.conditional_format("$A$1:$D${0}".format(table_location[2][2] + 1),
                                 {"type": "no_blanks",
                                  "format": normal_style
                                  })

    for i, width in enumerate(
            util.compare_unequal_len(util.get_col_widths(df_overall),
                                util.compare_unequal_len(util.get_col_widths(df_match_attr),
                                                    util.get_col_widths(df_source_prop)))):
        worksheet.set_column(i, i, width)


def extra_full_join(extra_join_condition):
    """
    Calculate an outer join for two tables give in configuration file.
    Join conditions are the same as in configuration file plus one from the list of not join conditions.

    :param dict extra_join_condition: new join condition with two keys(names of the tables)
    :return dataframe outer_join_df: new joined table
    """
    dict_join_conditions = util.create_dict_join_conditions(configuration)
    # merge existing and new join conditions together.
    for key in dict_join_conditions.keys():
        dict_join_conditions[key].append(extra_join_condition[key])
    list_of_sources = list(data.keys())
    full_join_df = pd.merge(data[list_of_sources[0]], data[list_of_sources[1]],
                            how="outer",
                            left_on=dict_join_conditions[list_of_sources[0]],
                            right_on=dict_join_conditions[list_of_sources[1]],
                            indicator=True)

    # create columns to indicate where the records come from
    full_join_df.loc[full_join_df['_merge'] == "both", 'source_both'] = 1
    # create new column 'source_both' with 1 for each value
    return full_join_df

def key_violation_warning(table, source_data, join_conditions):
    """
    :param table: name of the data source
    :param source_data: data frame with selected table
    :param join_conditions: column names used as a key
    :return:
    """
    original_size = len(source_data.drop_duplicates())
    with_keys_size = len(source_data[join_conditions].drop_duplicates())
    if original_size != with_keys_size:
        print("   Keys for join condition in " + table + " are NOT unique.")
        sys.exit(-1)

def full_join(data, configuration):
    """
    joins together the data frames contained within data,
    using the keys as specified in configuration.
    The column left_only, right_only, both indicate availability of the rows on which side of the join.

    Can be done when join_conditions in both tables are different.
    """

    dict_join_conditions = util.create_dict_join_conditions(configuration)
    list_of_sources = []

    for source in configuration['source_data']:
        if source['to_be_joined'] == 'True':
            list_of_sources.append(source['source_name'])

    # Create a warning message, if there is key violation in one of the tables
    for table in list_of_sources:
        key_violation_warning(table, data[table], dict_join_conditions[table])
    
    joined_df = pd.merge(data[list_of_sources[0]], data[list_of_sources[1]], how="outer",
                         left_on=dict_join_conditions[list_of_sources[0]],
                         right_on=dict_join_conditions[list_of_sources[1]],
                         indicator=True)

    joined_df['_merge'] = joined_df['_merge'].map(
        {'left_only': str(list_of_sources[0]),
         'right_only': str(list_of_sources[1]),
         'both': 'both'})

    # create columns to indicate where the records come from
    for key in data.keys():
        col_name = "source_" + key
        joined_df.loc[joined_df['_merge'] == key, col_name] = 1

    joined_df.loc[joined_df['_merge'] == "both", 'source_both'] = 1
    return joined_df