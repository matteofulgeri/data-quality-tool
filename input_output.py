import datetime
import os
import shutil
import time
import pandas as pd
from sqlalchemy import create_engine
import constants
import json
import utilities as util
import config_validation as v


# Read configuration file
def load_configuration(file_name):
#    v.validate(file_name)
    with open(file_name, "r", encoding='utf-8') as read_file:  
        data = json.load(read_file)
    return data

# Read data frame from binary 
def read_pickle(input_file):
    data_frame_from_pickle = pd.read_pickle(input_file)
    return data_frame_from_pickle

# Read data frame from text 
def read_csv(input_file, separate, qualifier, encoding):
    if len(encoding) == 0:
        encoding = constants.ENCODING_ISO
    if len(qualifier) > 0:
        data_frame_from_csv = pd.read_csv(input_file, sep=separate, encoding=encoding, quotechar=qualifier)
    else:
        data_frame_from_csv = pd.read_csv(input_file, sep=separate, encoding=encoding)
    # read_csv takes an encoding option to deal with files in different formats.
    return data_frame_from_csv


# Read data frame from excel 
def read_excel(input_file, sheet):
    data_frame_from_excel = pd.read_excel(input_file, sheet_name=sheet)
    return data_frame_from_excel

#Read Data from Database
def database_extraction(connection_data):
    #connection_data = source_data['db']
    query = connection_data['sql_code'] 
    if connection_data['driver'] == 'mssql+pyodbc':
        #This is the connection string to be used for MS SQL Server 
        connection_link = connection_data['driver'] + "://@" + connection_data['host'] + ":" + connection_data['port'] + "/" + connection_data['database'] + '?trusted_connection=yes&driver=ODBC+Driver+13+for+SQL+Server'
    elif connection_data['driver'] == 'postgresql':
        #This is the connection string to be used for Redshift and Mysql servers
        connection_link = connection_data['driver'] + "://" + connection_data['username'] + ":" + connection_data['password'] + "@" + \
                      connection_data['host'] + ":" + connection_data['port'] + "/" + connection_data['database']
    engine = create_engine(connection_link)
    data_frame = pd.read_sql_query(query, engine)  # Read SQL database table into a DataFrame.
    return data_frame


# Create dictionary using configuration file. Key: source_name.
# Value is a data frame from source data relevant for this source_name.
# Change date_format to standard format
def load_data(configuration):
    t = util.start_clock("- Loading Data: ", message_end = '\\n')
    data = {}
    data_frame = None
    #source_data = configuration['source_data'][0]
    for source_data in configuration['source_data']:
        t = util.start_clock("   ", variable = source_data['source_name'])
        if source_data['source_type'] == 'db':
            data_frame = database_extraction(source_data['db'])
        elif source_data['source_type'] == 'binary':
            data_frame = read_pickle(source_data['binary']['path'])
        elif source_data['source_type'] == 'text':
            data_frame = read_csv(source_data['text']['path'], source_data['text']['text_separator'], source_data['text']['text_qualifier'],encoding=source_data['text']['encoding'])
        elif source_data['source_type'] == 'excel':
            data_frame = read_excel(source_data['excel']['path'], source_data['excel']['excel_sheet'])
        elif source_data['source_type'] == 'script':
            data_frame = read_script(source_data['script']['path'])
        
        data[source_data['source_name']] = data_frame
        util.stop_clock(t)
    
    return data

def read_script(path):
    exec(open(path).read(),globals())
    return data

# Create an excel from a data frame.
def create_excel_from_df(df, excel_name, sheet_name):
    writer = pd.ExcelWriter(excel_name)
    df.to_excel(writer, sheet_name)
    writer.save()

# Wrapper for save files
def save_input(data_frame, type, name):
    if type == "text":
        save_to_csv(data_frame, name)
    elif type == "binary":
        save_to_pickle(data_frame, name)
        
# Save data frame to binary file
def save_to_pickle(data_frame, name):
    data_frame.to_pickle(name)

# Save data frame to binary text
def save_to_csv(data_frame, name):
    data_frame.to_csv(name, sep='\t')

# create backup zip file in output folder
def create_backup(backup_file_name, FILE_NAME, configuration, data):
    cond_save_config = configuration['execution']['create_backup_zip']['save_configuration'] == 'True'
    cond_save_source = configuration['execution']['create_backup_zip']['save_sourcecode'] == 'True'
    cond_save_input = any([input_to_save['active'] == 'True' for input_to_save in configuration['execution']['create_backup_zip']['save_input']])

    if any([cond_save_config, cond_save_input, cond_save_source]):
        t = util.start_clock("- Creating backup ", message_end='\\n')
        os.makedirs('./backup/', exist_ok=True)
        data_dir_name = os.path.basename(os.path.abspath(os.path.join(os.path.dirname(FILE_NAME), '..')))
        if cond_save_source:
            save_sourcecode()

        if cond_save_config:
            save_configuration(FILE_NAME, data_dir_name)

        if cond_save_input:
            save_inputs(FILE_NAME, configuration, data)

        zip_backup(backup_file_name)
    else:
        print('- Backup Skipped')


def zip_backup(backup_file_name):
    t = util.start_clock("   Creating zip files of source code")

    shutil.make_archive(backup_file_name, 'zip', './backup/')
    shutil.rmtree('./backup/')

    util.stop_clock(t)


def save_configuration(FILE_NAME, data_dir_name):
    t = util.start_clock("   Saving config file ")
    os.makedirs('./backup/' + data_dir_name + '/0_configuration', exist_ok=True)
    source_file = FILE_NAME
    dest_file = './backup/' + data_dir_name + '/0_configuration/' + os.path.basename(FILE_NAME)

    shutil.copy2(source_file, dest_file)
    util.stop_clock(t)


def save_sourcecode():
    t = util.start_clock("   Saving source code ")
    included_extensions = ['py', 'json']
    files = [os.path.join(os.getcwd(), f) for f in os.listdir('.') if any(f.endswith(ext) for ext in included_extensions)]

    for file in files:
        shutil.copy2(os.path.join(os.getcwd(), file), os.path.join(os.getcwd(), 'backup', os.path.basename(file)))
    util.stop_clock(t)


def save_inputs(FILE_NAME, configuration, data):
    # input_to_save = configuration['execution']['save_input'][0]
    t = util.start_clock("   Saving Input:", message_end='\\n')
    files_to_copy = []
    #input_to_save = configuration['execution']['create_backup_zip']['save_input'][1]
    for input_to_save in configuration['execution']['create_backup_zip']['save_input']:
        if input_to_save['active'] == 'True':
            t1 = util.start_clock("     Saving", input_to_save['source_name'])
            if input_to_save['destination_type'] == 'binary':
                file_name = configuration['execution']['create_backup_zip']['path'] + input_to_save['source_name'] + "_backup - " + datetime.datetime.today().strftime('%Y-%m-%d %H.%M.%S') + ".pkl"
                files_to_copy.append(file_name)
            else:
                file_name = configuration['execution']['create_backup_zip']['path'] + input_to_save['source_name'] + "_backup - " + datetime.datetime.today().strftime('%Y-%m-%d %H.%M.%S') + ".csv"
                files_to_copy.append(file_name)
            save_input(data[input_to_save['source_name']], input_to_save['destination_type'], file_name)
            util.stop_clock(t1)

    dest_folder = './backup/source_data/'
    os.makedirs(dest_folder, exist_ok=True)

    for file in files_to_copy:
        shutil.move(file, dest_folder + os.path.basename(file))