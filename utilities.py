import time
import constants

def start_clock(message, variable = None, message_end = ''):
    start_time = time.time()
    if variable:
        message_to_print = "print(\"" + message + "\",\"" + variable  + "\", end='"+ message_end + "', flush=True)"
    else:
        message_to_print = "print(\"" + message + "\",\"" + "\", end='"+ message_end + "', flush=True)"
    exec(message_to_print)
    return start_time


def stop_clock(start_time, message = None):
    if message: 
        print(message + " " + str(round((time.time() - start_time), 1)), end = '\n', flush = True)
    else: 
        print(" [Completed in " + str(round((time.time() - start_time), 1)) + " secs]", end = '\n', flush = True)
    

def definition_to_original_column_name(configuration, data_soruce_name, searched_field):
    for definition in configuration['analysis_definitions']:
        if definition['label'] == searched_field:
            for column in definition['source_columns']:
                if column['source_name'] == data_soruce_name:
                    return column['source_column']

def create_dict_join_conditions(configuration):
    """
    generate a dictionary to structure the join conditions to be used for the analysis
    """
    dict_join_conditions = {}
    for dimensions in configuration['analysis_definitions']:
        if ((dimensions["attribute_type"] == "dimension") & (dimensions["active"] == 'True')):
            for sources in dimensions['source_columns']:
                dict_join_conditions.setdefault(sources['source_name'], []).append(sources['source_column'])
    return dict_join_conditions


def definition_to_labels(definitions, configuration):
    """
    retrieve the couple of original field names, given the analysis defintion.
    
    definetions:    list of elements that needs to be searched
    configuration:  configuration file
    returns: for every element specified in definitions, return the 2 field names from the input data
    """
    list = []
    full_analysis_definition = configuration['analysis_definitions']
    for definition_unit in definitions:  # for each element in the definition list
        for single_dimension_definition in full_analysis_definition:
            if single_dimension_definition['label'] == definition_unit:
                for source_column in single_dimension_definition['source_columns']:
                    list.append(source_column['source_column'])
    # if lenght(list) == lenght(definitions) / 2 -> ok else "not ok"
    try:
        if len(list) != len(definitions) * 2:
            raise ValueError('Length of list and definition are mismatch')
    except ValueError as err:
        print(err)
        raise
    return list

def label_to_definition_single_source(definitions, configuration):
    for definition in configuration['analysis_definitions']:
        for column in definition['source_columns']:
            if column['source_column'] == definitions:
                return definition['label']
    
def cleanup_excel_sheetname(sheet_name):
    """
    reducing the amount of character to the maximum allowe for an excel sheet. 
    also remove the not allowed characters, by replacing them with empty spaces
    """
    if len(sheet_name) > 31:
            sheet_name = sheet_name[0:30]
            
    sheet_name = sheet_name.replace('[', '').replace(']', '').replace(':', '').replace('*', '').replace('?', '').replace('/', '').replace('\\', '')
    return sheet_name


def get_col_widths(dataframe):
    resize_factor = constants.WIDTH_FACTOR
    # get width of all col
    width_list_colname = [len(col) for col in dataframe.columns]
    width_list_data = []
    for index in dataframe.columns: 
        width_list_data.append(dataframe[index].map(lambda x: len(str(x))).max())
        
    width_list = width_list_colname
    
    for index, value in enumerate(width_list_colname):
        if width_list_data[index] > value:
            width_list[index] = width_list_data[index]
        else: 
            width_list[index] = value
            
    # return list of width with 10% padding
    return [int(round(x * resize_factor)) for x in width_list]


def isnull_column(joined_table, output_definition):
    # implementing the isnull column functionality of SQL. 
    # merging the values availavle in 2 colums in 1. 
    for dimension in output_definition['dimensions']:
        dimension_pairs = definition_to_labels([dimension], configuration)
        #Adding a column representing the isnull of hte combination of the couples
        joined_table[dimension + "_agg"] = joined_table[dimension_pairs[0]].fillna(joined_table[dimension_pairs[1]])
        #dropping the couples of existing columns
        joined_table.drop(dimension_pairs, axis = 1, inplace = True)
    return joined_table


def compare_unequal_len(a, b):
    if len(a) <= len(b):
        shorter = a
        longer = b
    else:
        shorter = b
        longer = a

    output = []
    for i in list(range(0, len(shorter))):
        if shorter[i] >= longer[i]:
            output.append(shorter[i])
        else:
            output.append(longer[i])

    for i in list(range(len(shorter), len(longer))):
        output.append(longer[i])
    return output


def names_extra_joins(configuration):
    """
    Look up the structure of JSONobject configuration["analysis_definitions"] and
    creates a list with "label" as a name, where join condition has an empty field "" in configuration file.

    :return list names_extra_joins:
    """
    names_extra_joins = []
    for dimensions in configuration['analysis_definitions']:
        if dimensions["attribute_type"] == "":
            names_extra_joins.append(dimensions["label"])
    return names_extra_joins


def create_extra_join_conditions(configuration):
    """
    Look up the structure of JSONobject configuration["analysis_definitions"] and
    creates a dictionary, where key is a source name and value is a list of dimensions
    which is declared as join condition with an empty field "" in configuration file.

    :return list of dictionaries: list if dictionaries, where each dictionary containstwo key s
    for each data table and extra join condition. One new join condition per each dictionra
    """
    list_extra_join_conditions = []
    for dimensions in configuration['analysis_definitions']:
        if dimensions["attribute_type"] == "":
            dict_extra_join_conditions = {}
            for sources in dimensions['source_columns']:
                dict_extra_join_conditions[sources['source_name']] = sources['source_column']
            list_extra_join_conditions.append(dict_extra_join_conditions)
    return list_extra_join_conditions


def parse_dates(configuration):
    for analysis_definitions in configuration['analysis_definitions']:
        for column in analysis_definitions['source_columns']:
            if len(column['date_format']) > 0:
                source_name = column['source_name']
                source_column = column['source_column']
                input_format = column['date_format']
                data[source_name][source_column] = pd.to_datetime(
                    data[source_name][source_column], format=input_format).dt.strftime(constants.OUTPUT_DATE_FORMAT)


def add_suffix(data, configuration):
    # Adding Suffix to the headers
    for source in list(data.keys()):
        data[source] = data[source].add_suffix('_' + source)

    # Adding Suffix in the configuration as well to allign the information
    for analysis_definitions in configuration['analysis_definitions']:
        for column in analysis_definitions['source_columns']:
            source_name = column['source_name']
            column['source_column'] = column['source_column'] + '_' + source_name


def cast_df_to_string(data, configuration):
    # Casting joining condition of dataframe to string
    dict_join_conditions = create_dict_join_conditions(configuration)
    for source_name in dict_join_conditions:
        data[source_name][dict_join_conditions[source_name]] = data[source_name][dict_join_conditions[source_name]].astype(str)


def list_of_field_by_type(configuration, data_source_name, attribute_type):
    """ 
    Generate a list of fields for one of the 2 data sources besed on the attribute specified as a paramenter
    """
    list_of_dimension = []
    if attribute_type == 'dimension':
        for analysis_definition_unit in configuration['analysis_definitions']:
            if analysis_definition_unit['active'] == 'True' and analysis_definition_unit['attribute_type'] == 'dimension':
                if analysis_definition_unit['source_columns'][0]['source_name'] == data_source_name:
                    list_of_dimension.append(analysis_definition_unit['source_columns'][0]['source_column'])
                elif analysis_definition_unit['source_columns'][1]['source_name'] == data_source_name:
                    list_of_dimension.append(analysis_definition_unit['source_columns'][1]['source_column'])    
    elif attribute_type == 'measure':
        for analysis_definition_unit in configuration['analysis_definitions']:
            if analysis_definition_unit['active'] == 'True' and analysis_definition_unit['attribute_type'] == 'measure':
                if analysis_definition_unit['source_columns'][0]['source_name'] == data_source_name:
                    list_of_dimension.append(analysis_definition_unit['source_columns'][0]['source_column'])
                elif analysis_definition_unit['source_columns'][1]['source_name'] == data_source_name:
                    list_of_dimension.append(analysis_definition_unit['source_columns'][1]['source_column'])    
    
    return list_of_dimension


def measure_to_column_names(output_definition, output, measure):
    """
    Given a measure name and a definition of the output, is returning a couple of indices corresponding to
    the colum names (in excel format: A, AC, ..) where the actual measure are located into the output structure
    """
    column_list = list(output.columns)
    result = []
    
    # For each measure specified in output definition
    # measure = list(output_definition['measures'].keys())[0]
    index = 0
    #for eavery measure can appear multiple aggregation funcions. for each aggregation function we are searching the reference 
    for aggregation_funcion in output_definition['measures'][measure]:
        #The first reference of the measures we are searching, corresponding to the value on the left table 
        left_column_label = list(filter( lambda x: measure in x, column_list))[index]
        #The second reference of the measures we are searching, corresponding to the value on the right table 
        right_column_label = list(filter( lambda x: measure in x, column_list))[index+1]
        
        column_position_left = column_list.index(left_column_label)
        column_position_right = column_list.index(right_column_label)
        
        result.append(int_to_excel_column(column_position_left +1))
        result.append(int_to_excel_column(column_position_right+1))
        #increasing the index for the next iteration 
        index = index + 4 
        
    return result
            
def int_to_excel_column(index, base = 26):
    index -= 1 
    int_division = index // base 
    rest_division = index % base 
    
    if int_division > 0: 
        return int_to_excel_column(int_division, base) + int_to_excel_column(rest_division +1, base)
    else: 
        return str(chr(ord('A') + index))


def Mask_Or_Drop_Successive_Identical_Values(df, drop = False, 
                                             keep_first = True,
                                             axis = 0, how = 'all'):

    '''
    #Function built with the help of:
    # 1) https://stackoverflow.com/questions/48428173/how-to-change-consecutive-repeating-values-in-pandas-dataframe-series-to-nan-or
    # 2) https://stackoverflow.com/questions/19463985/pandas-drop-consecutive-duplicates

    Input:
    df should be a pandas.DataFrame of a a pandas.Series
    Output:
    df of ts with masked or droped values
    '''

    # Mask keeping the first occurence
    if keep_first == True:
        df = df.mask(df.shift(1) == df)
    # Mask including the first occurence
    elif keep_first == False:
        df = df.mask((df.shift(1) == df) | (df.shift(-1) == df))

    # Only mask the values (e.g. become 'NaN')
    if drop == False:
        return df

    # Drop the values (e.g. rows are deleted)
    else:
        return df.dropna(axis = axis, how = how)