##### data_quality_tool.py

#def remove_suffix(source_name):
#    #source_name = input_to_save['source_name']
#    data_frame_to_clean = data[source_name]
#
#    #Cast df to string to avoid any type issue while saving the data
#    data_frame_to_clean = data_frame_to_clean.astype(str)
#
#    renamed_columns = list(data_frame_to_clean.columns)
#    for i in range(0, len(renamed_columns)):
#        renamed_columns[i] = renamed_columns[i].replace('_' + source_name, '')
#
#    data_frame_to_clean.columns = renamed_columns
#    return data_frame_to_clean
#
#def generate_alternative_join_conditions(configuration):
#    # Taking all the elements in join condition
#    join_conditions = set()
#    for dimensions in configuration['analysis_definitions']:
#        if dimensions["is_join_condition"] == 'True':
#            join_conditions.add(dimensions['label'])
#
#    join_condition_list = list()
#
#    for i in range(1, len(join_conditions)):
#        join_condition_list = join_condition_list + list(itertools.combinations(join_conditions, i))
#
#    return join_condition_list
#
#def alternative_joins(data, configuration):
#    t = util.start_clock("- Generating alternative join conditions: ", message_end='\\n')
#    join_conditions = generate_alternative_join_conditions(configuration)
#
#    tables_to_join = []
#    for data_source_name in configuration['source_data']:
#        if data_source_name['to_be_joined'] == 'True':
#            tables_to_join.append(data_source_name)
#
#    left_source = tables_to_join[0]['source_name']
#    right_source = tables_to_join[1]['source_name']
#
#    aggregate_tables = {}
#    results = pd.DataFrame(
#        columns=['Id', 'Join', '# Records Matching in both', '# Records Matching ' + left_source + ' only',
#                 '# Records Matching ' + right_source + ' only', '% Records Matching',
#                 '% Records in ' + left_source + ' only', '% Records in ' + right_source + ' only'])
#
#    results_joined_tables = {}
#
#    for alternative_join in join_conditions:
#        joins = list(alternative_join)
#        t = util.start_clock("   ", variable=str(joins), message_end='\\n')
#        groupBy_columns = {}
#
#        # Calcultinf the group by columns belonging to the first data soruce
#        groupBy_columns[left_source] = definition_to_labels_single_source(joins, configuration, left_source)
#
#        # Calculting the group by columns belonging to the second data soruce
#        groupBy_columns[right_source] = definition_to_labels_single_source(joins, configuration, right_source)
#
#        # grouping first table
#        aggregate_tables[left_source] = data[left_source].groupby(groupBy_columns[left_source],
#                                                                  as_index=False).size().reset_index(
#            name='# Records ' + left_source).astype(str)
#
#        # grouping second table
#        aggregate_tables[right_source] = data[right_source].groupby(groupBy_columns[right_source],
#                                                                    as_index=False).size().reset_index(
#            name='# Records ' + right_source).astype(str)
#
#        left_join_keys = groupBy_columns[left_source]  # + ['# Records '+ left_source]
#        right_join_keys = groupBy_columns[right_source]  # + ['# Records '+ left_source]
#
#        # joining aggregated tables
#        full_join_df = pd.merge(aggregate_tables[left_source], aggregate_tables[right_source], how="outer",
#                                left_on=left_join_keys,
#                                right_on=right_join_keys,
#                                indicator=True)
#
#        full_join_df['_merge'] = full_join_df['_merge'].map(
#            {'left_only': left_source,
#             'right_only': right_source,
#             'both': 'both'})
#
#        # Calculating and storing the overall the Results
#        join_label = ' '.join(alternative_join)
#        index = len(results.index) + 1
#
#        num_matching = len(full_join_df[full_join_df['_merge'] == 'both'])  # elements matching completely
#        num_left_only = len(
#            full_join_df[full_join_df['_merge'] == left_source])  # elements available just on the left data source
#        num_right_only = len(
#            full_join_df[full_join_df['_merge'] == right_source])  # elements available just on the left data source
#
#        total_records = num_matching + num_left_only + num_right_only
#
#        percentage_matching = round(num_matching / total_records, 2)
#        percentage_left_only = round(num_left_only / total_records, 2)
#        percentage_right_only = round(num_right_only / total_records, 2)
#        # Generating the result line containing overall statistincs
#        result_line = [index, join_label, num_matching, num_left_only, num_right_only, percentage_matching,
#                       percentage_left_only, percentage_right_only]
#
#        results.loc[index] = result_line
#        results_joined_tables[index] = full_join_df
#
#    final_results = [results, results_joined_tables]
#    return final_results
#
#def write_alternative_joins(results, excelWriter):
#    t = util.start_clock("   Saving Alternative Join Results")
#    overall_results = results[0]
#    data_frames_results = results[1]
#
#    write_output_to_excel(overall_results, excelWriter, sheet_name='Alternative join')
#
#    if configuration['execution']['output']['alternative_join_condition']['save_tables'] == 'True':
#        for i in range(1, len(data_frames_results) + 1):
#            df = data_frames_results[i]
#            write_output_to_excel(df, excelWriter, sheet_name="Alternative join #" + str(i))
#    util.stop_clock(t)
#
#def definition_to_labels_single_source(definitions, configuration, source_name):
#    list = []
#    # definitions = joins
#    full_analysis_definition = configuration['analysis_definitions']
#    for definition_unit in definitions:  # for eache element in the definition list
#        for single_dimension_definition in full_analysis_definition:
#            if single_dimension_definition['label'] == definition_unit:
#                for source_column in single_dimension_definition['source_columns']:
#                    if source_column['source_name'] == source_name:
#                        list.append(source_column['source_column'])
#    # if lenght(list) == lenght(definitions)  -> ok else "not ok"
#    try:
#        if len(list) != len(definitions):
#            raise ValueError('Length of list and definition are mismatch, single source')
#    except ValueError as err:
#        print(err)
#        raise
#    return list