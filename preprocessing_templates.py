#substitute values in a given column
Legacy['Brand'] = Legacy['Brand'].replace('TYKERB','TYVERB')

#remap values values in a given column (can act as mupliple renames)
df['action_mapped'] = df['action'].map({'Click': 'Clicked','HardBounce': 'HardBounce','Send': 'Sent','SoftBounce': 'SoftBounce','Unsubscribed': 'Unsubscribed','Open': 'Viewed'})

#Remap with case with null values 
Legacy['Product'] = Legacy['Product'].map({'SAS LAR':'SANDOSTATINE','TAFINLAR':'TAFMEK'}).fillna(Legacy['Brand'])

#Upper case values in a given column
Legacy['Channels'] = Legacy['Channels'].str.upper()

#Filter out some data. String. Single condition. "IN".
df[df['TEAM_NAME'].isin(['DE PROLIA SC','DE SC GP'])]

#Filter out some data. String. Single condition.
Legacy = Legacy[Legacy['Name'] != 'CREDIT NOTE DUMMY CUSTOMER']

#Filter data. date. Single Condition.
Legacy['DateYM'] = Legacy[Legacy['DateYM'] >= time.strptime('20180101', '%Y%m%d')]

#Filter Data. dates. Multiple condition: Convert string into a date, apply double restriction.
SI['period_datetime'] = SI['period'].apply(lambda x: time.strptime(x,'%Y%m%d'))\nSI = SI[(SI['period_datetime'] <= time.strptime('06.07.2019', '%d.%m.%Y')) & (SI['period_datetime'] >= time.strptime('01.01.2018', '%d.%m.%Y'))]

#Filter Data. string. Multiple condition:
SI = SI[(SI['net_sales'] != 0) & (SI['units'] !=0) ]

#Trim and trailing space
Legacy['Account'] = Legacy['Account'].str.strip()

#Group by: Convert string to floats to enable to aggregation. specify the dimension to be used for the aggregation, and colculate the groupping
Legacy[['Net Sales (GBP)', 'Volume Sales (PC)','PPRS Netural Cash(GBP)']] = Legacy[['Net Sales (GBP)', 'Volume Sales (PC)','PPRS Netural Cash(GBP)',]].applymap(lambda x: float(x))\ngroupby_cols = ['Channels', 'Brand', 'DateYM','Territory']\naggr_cols = {'PPRS Netural Cash(GBP)' : ['sum'], 'Net Sales (GBP)' : ['sum'], 'Volume Sales (PC)' : ['sum']}\nrename_cols = groupby_cols + ['pprs_neutral_cash','net_sales','volume']\nLegacy = Legacy.groupby(groupby_cols, as_index=False).agg(aggr_cols)\nLegacy.columns = rename_cols

#Convert string into a date. YYYYMM format
SI['DateYM'] = SI['period_datetime'].dt.strftime('%Y%m')

#Simple inline function to a given column: right(6)
SI['DateYM'] = SI['period'].apply(lambda x: str(x)[:6])

#Take a part of the string given a column (similar as above point) 
df['yearmonth'] = df['EVENT_START_DATE_TIME'].str[0:7]

#Conditional inline function to a given column
df['team_name'] = df['ALIGNMENT_NAME'].apply(lambda x: x[0:x.find(' - ')-7] if x.find(' - ') > 0 else x[0:len(x)-6] )

#inline function to a multidimensional structure
df['month',year'].apply(lambda x: 'aaa' if x[0] < x[1] else 'bbb', axis = 1)

#numpy equivalent of inline function with if
df['new_col'] = np.where(df['col_to_compare']=='AAA', 'then_result', 'else_result')

#merging files together (from multiple data soruces) 
Legacy4 = Legacy4.drop(['Unnamed: 7'],axis = 1)\nLegacy['Sales_line'] = str('51')\nLegacy1['Sales_line'] = str('05')\nLegacy2['Sales_line'] = str('17')\nLegacy3['Sales_line'] = str('56')\nLegacy4['Sales_line'] = str('85')\nLegacy = pd.concat([Legacy, Legacy1, Legacy2, Legacy3, Legacy4], axis = 0, sort = True)

#Reorganize the data by joining them
Legacy_cash = Legacy[Legacy['Datentypen'] == 'Umsatz€']\nLegacy_cash = Legacy_cash.drop(['Datentypen'], axis = 1)\nLegacy_cash.columns = ['Area Code', 'Area', 'ChefCode', 'Hierarchy', 'Präparate', 'Sales_line','net_sales']\nLegacy_vol = Legacy[Legacy['Datentypen'] == 'EH']\nLegacy_vol = Legacy_vol.drop(['Datentypen'], axis = 1)\nLegacy_vol.columns = ['Area Code', 'Area', 'ChefCode', 'Hierarchy', 'Präparate', 'Sales_line','unit']\nLegacy = pd.merge(Legacy_cash, Legacy_vol, how = 'outer' ,on = ['Area Code', 'Area', 'ChefCode', 'Hierarchy', 'Präparate', 'Sales_line'])

#cast a column to string
Legacy['KundeNr.'] = Legacy['KundeNr.'].astype(str)

#Pivoting data 
groupby_cols = list(Legacy.columns.difference(['Weekly_Sales','Measure']))\ngroupby_cols.append('Measure')\nLegacy = Legacy.groupby(groupby_cols, as_index=False).agg('sum')\nLegacy.set_index(groupby_cols, inplace =True, verify_integrity = True)\nLegacy = Legacy.unstack().reset_index()\ngroupby_cols.remove('Measure')\ngroupby_cols.append('Net Sales')\ngroupby_cols.append('Volumes')\nLegacy.columns = groupby_cols",